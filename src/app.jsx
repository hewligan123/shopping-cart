import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import store from './store/store';
import Page from './components/Page/Page';
import { cartListener } from './lib/cartStorage';

export default (mountPoint) => {
    cartListener(store);
    render(
        <Provider store={store}><Page /></Provider>,
        mountPoint,
    );
};
