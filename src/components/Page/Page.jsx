import React, { Component } from 'react';

import {
    Jumbotron,
    Container,
    Row,
    Col,
} from 'reactstrap';

import ProductListContainer from '../ProductList/ProductListContainer';
import CartListContainer from '../CartList/CartListContainer';

class Page extends Component {
    render() {
        return (
            <Container>
                <Jumbotron>
                    <h1>Shopping Cart</h1>
                </Jumbotron>
                <Row>
                    <Col>
                        <ProductListContainer />
                    </Col>
                    <Col>
                        <CartListContainer />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Page;
