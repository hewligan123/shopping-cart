import React, { Component, Fragment } from 'react';

import PropTypes from 'prop-types';

import { map } from 'ramda';

import ProductItem from './ProductItem';

class ProductList extends Component {
    render() {
        const { products, addItem } = this.props;

        return (
            <Fragment>
                <h2>Available Items</h2>
                {
                    map(
                        item => <ProductItem key={item.key} item={item} addItem={addItem} />,
                        products,
                    )
                }
            </Fragment>
        );
    }
}

ProductList.propTypes = {
    products: PropTypes.array.isRequired,
    addItem: PropTypes.func.isRequired,
};

export default ProductList;
