import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Card,
    CardHeader,
    CardBody,
    CardText,
    Button,
} from 'reactstrap';

import formatPrice from '../../lib/formatPrice';

class ProductItem extends Component {
    constructor(props) {
        super(props);
        this.addToCart = this.addToCart.bind(this);
    }

    addToCart() {
        const { item, addItem } = this.props;
        addItem(item.key);
    }

    render() {
        const { item } = this.props;

        return (
            <Card className="mb-3 text-center">
                <CardHeader>{item.name}</CardHeader>
                <CardBody>
                    <CardText>
                        Price:&nbsp;
                        {formatPrice(item.price)}
                    </CardText>
                    <Button onClick={this.addToCart}>Add to cart</Button>
                </CardBody>
            </Card>
        );
    }
}

ProductItem.propTypes = {
    item: PropTypes.shape({
        key: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.number,
    }).isRequired,
    addItem: PropTypes.func.isRequired,
};

export default ProductItem;
