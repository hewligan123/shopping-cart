import { connect } from 'react-redux';

import { addToCart } from '../../store/cart/cartActionCreators';

import createListFromProducts from '../../lib/createListFromProducts';

import ProductList from './ProductList';

const mapStateToProps = (state) => {
    return {
        products: createListFromProducts(state.products),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addItem(key) {
            dispatch(addToCart(key));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
