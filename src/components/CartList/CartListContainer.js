import { connect } from 'react-redux';

import { map, product, reduce } from 'ramda';

import { removeFromCart } from '../../store/cart/cartActionCreators';

import CartList from './CartList';

const createCartList = state => map(
    (key) => {
        return {
            key,
            amount: state.cart.cartItemCounts[key],
            total: product([
                state.products[key].price,
                state.cart.cartItemCounts[key],
            ]),
            ...state.products[key],
        };
    },
    state.cart.cartItems,
);

const mapStateToProps = (state) => {
    const cart = createCartList(state);
    const total = reduce(
        (sum, item) => sum + item.total,
        0,
        cart,
    );

    return {
        cart: createCartList(state),
        total,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeItem(key) {
            dispatch(removeFromCart(key));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartList);
