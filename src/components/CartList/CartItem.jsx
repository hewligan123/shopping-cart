import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    ListGroupItem,
    ListGroupItemHeading,
    Button,
} from 'reactstrap';

import formatPrice from '../../lib/formatPrice';

class CartItem extends Component {
    constructor(props) {
        super(props);
        this.remove = this.remove.bind(this);
    }

    remove() {
        const { item, removeItem } = this.props;

        removeItem(item.key);
    }

    render() {
        const { item } = this.props;
        return (
            <ListGroupItem>
                <ListGroupItemHeading>
                    {item.name}
                </ListGroupItemHeading>
                <div>
                    <p>
                        Price:&nbsp;
                        {formatPrice(item.price)}
                    </p>
                    <p>
                        Amount:&nbsp;
                        {item.amount}
                    </p>
                    <p>
                        Total:&nbsp;
                        {formatPrice(item.total)}
                    </p>
                    <p>
                        <Button onClick={this.remove}>Remove from cart</Button>
                    </p>
                </div>
            </ListGroupItem>
        );
    }
}

CartItem.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        key: PropTypes.string,
        amount: PropTypes.number,
        price: PropTypes.number,
        total: PropTypes.number,
    }).isRequired,
    removeItem: PropTypes.func.isRequired,
};

export default CartItem;
