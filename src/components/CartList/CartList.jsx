import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { map } from 'ramda';
import { ListGroup } from 'reactstrap';

import CartItem from './CartItem';

import formatPrice from '../../lib/formatPrice';

class CartList extends Component {
    render() {
        const { cart, removeItem, total } = this.props;

        return (
            <Fragment>
                <h2>Cart</h2>
                <ListGroup>
                    {map(
                        item => (<CartItem item={item} key={item.key} removeItem={removeItem} />),
                        cart,
                    )}
                </ListGroup>
                <h3>
                    Order Total:&nbsp;
                    {formatPrice(total)}
                </h3>
            </Fragment>
        );
    }
}

CartList.propTypes = {
    cart: PropTypes.array.isRequired,
    removeItem: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
};

export default CartList;
