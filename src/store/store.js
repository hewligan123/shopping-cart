import { createStore, combineReducers } from 'redux';

import productsReducer from './products/productsReducer';
import cartReducer from './cart/cartReducer';

/* eslint-disable no-underscore-dangle */
const store = createStore(
    combineReducers({ products: productsReducer, cart: cartReducer }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
/* eslint-enable */

export default store;
