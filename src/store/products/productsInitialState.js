import { reduce, toLower } from 'ramda';

import products from '../../lib/products';

const createProductMap = reduce(
    (prods, item) => {
        return { [toLower(item.name)]: item, ...prods };
    },
    {},
);

export default createProductMap(products);
