import productsInitialState from './productsInitialState';

const productsReducer = () => productsInitialState;

export default productsReducer;
