import { append, without } from 'ramda';

import { ADD_TO_CART, REMOVE_FROM_CART } from './cartActionTypes';

import cartInitialState from './cartInitialState';

const addItemToCart = (prevState, key) => {
    const cartItems = prevState.cartItemCounts[key] > 0
        ? prevState.cartItems
        : append(key, prevState.cartItems);

    const newItemCount = (prevState.cartItemCounts[key] || 0) + 1;
    const cartItemCounts = { ...prevState.cartItemCounts, [key]: newItemCount };

    return {
        cartItems,
        cartItemCounts,
    };
};

const removeItemFromCart = (prevState, key) => {
    const newItemCount = prevState.cartItemCounts[key] - 1;

    const cartItemCounts = { ...prevState.cartItemCounts, [key]: newItemCount };

    const cartItems = newItemCount
        ? prevState.cartItems
        : without([key], prevState.cartItems);

    return {
        cartItems,
        cartItemCounts,
    };
};

const cartReducer = (prevState = cartInitialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            return addItemToCart(prevState, action.payload);

        case REMOVE_FROM_CART:
            return removeItemFromCart(prevState, action.payload);

        default:
            return prevState;
    }
};

export default cartReducer;
