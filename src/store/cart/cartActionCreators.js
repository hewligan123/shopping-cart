import { ADD_TO_CART, REMOVE_FROM_CART } from './cartActionTypes';

export const addToCart = (itemKey) => {
    return {
        type: ADD_TO_CART,
        payload: itemKey,
    };
};

export const removeFromCart = (itemKey) => {
    return {
        type: REMOVE_FROM_CART,
        payload: itemKey,
    };
};
