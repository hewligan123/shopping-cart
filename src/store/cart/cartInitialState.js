import { getStoredCart } from '../../lib/cartStorage';

const cartInitialState = {
    cartItems: [],
    cartItemCounts: {},
};

export default (getStoredCart() || cartInitialState);
