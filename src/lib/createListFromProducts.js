import { keys, reduce, append } from 'ramda';

const createListFromProducts = productMap => reduce(
    (pList, key) => append({ key, ...productMap[key] }, pList),
    [],
    keys(productMap),
);

export default createListFromProducts;
