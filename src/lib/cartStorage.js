const STORE_KEY = 'STORE';

let previousCart;
let warningSent = false;

export const getStoredCart = () => {
    try {
        return JSON.parse(localStorage.getItem(STORE_KEY));
    } catch (e) {
        return null;
    }
};

const storeCart = (cart) => {
    try {
        localStorage.setItem(STORE_KEY, JSON.stringify(cart));
    } catch (e) {
        if (!warningSent) {
            /* eslint-disable no-console */
            console.warn('Local Storage not available. Cart will not survive refresh');
            /* eslint-enable */
            warningSent = true;
        }
    }
};

export const cartListener = (store) => {
    store.subscribe(() => {
        const { cart } = store.getState();

        if (cart !== previousCart) {
            previousCart = cart;
            storeCart(cart);
        }
    });
};
