/* globals describe, it */

import { expect } from 'chai';

import { addToCart, removeFromCart } from '../../../src/store/cart/cartActionCreators';
import { ADD_TO_CART, REMOVE_FROM_CART } from '../../../src/store/cart/cartActionTypes';

describe('cartActionCreators', () => {
    it('Should create add to cart actions', () => {
        const testKey = 'TEST_KEY';
        const action = addToCart(testKey);

        expect(action).to.deep.equal({
            type: ADD_TO_CART,
            payload: testKey,
        });
    });

    it('Should create remove from cart actions', () => {
        const testKey = 'TEST_KEY';
        const action = removeFromCart(testKey);

        expect(action).to.deep.equal({
            type: REMOVE_FROM_CART,
            payload: testKey,
        });
    });
});
