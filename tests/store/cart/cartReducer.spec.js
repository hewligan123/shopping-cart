/* globals describe, it */

import { expect } from 'chai';

import cartReducer from '../../../src/store/cart/cartReducer';
import { addToCart, removeFromCart } from '../../../src/store/cart/cartActionCreators';

describe('cartReducer', () => {
    it('Should return an initial state', () => {
        const initialState = cartReducer(undefined, {});

        expect(typeof initialState).to.equal('object');
    });

    it('Should add items to the cart', () => {
        const testKey = 'TEST_KEY';
        const testAction = addToCart(testKey);
        const state = cartReducer(undefined, testAction);

        expect(state.cartItems.length).to.equal(1);
    });

    it('Should remove items from the cart', () => {
        const testKey = 'TEST_KEY';
        const prevState = {
            cartItems: [testKey],
            cartItemCounts: { [testKey]: 1 },
        };
        const testAction = removeFromCart(testKey);

        const state = cartReducer(prevState, testAction);

        expect(state.cartItems.length).to.equal(0);
    });
});
