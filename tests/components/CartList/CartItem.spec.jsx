/* globals describe, it, beforeEach, afterEach */

import React from 'react';

import { expect } from 'chai';
import sinon from 'sinon';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import CartItem from '../../../src/components/CartList/CartItem';

configure({ adapter: new Adapter() });

describe('CartItem', () => {
    const testProps = {
        item: {
            name: 'TEST_NAME',
            key: 'TEST_KEY',
            amount: 2,
            price: 1.2,
            total: 2.4,
        },
        removeItem: () => {},
    };

    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('Should display the name of the item', () => {
        const cartItem = shallow(
            <CartItem item={testProps.item} removeItem={testProps.removeItem} />,
        );

        expect(cartItem.contains(testProps.item.name)).to.equal(true);
    });

    it('Should display the amount of the item', () => {
        const cartItem = shallow(
            <CartItem item={testProps.item} removeItem={testProps.removeItem} />,
        );

        expect(cartItem.contains(testProps.item.amount)).to.equal(true);
    });

    it('Should display the price of the item', () => {
        const cartItem = shallow(
            <CartItem item={testProps.item} removeItem={testProps.removeItem} />,
        );

        expect(cartItem.contains('$1.20')).to.equal(true);
    });

    it('Should display the total price of the item', () => {
        const cartItem = shallow(
            <CartItem item={testProps.item} removeItem={testProps.removeItem} />,
        );

        expect(cartItem.contains('$2.40')).to.equal(true);
    });

    it('Should call the remove item function when the remove button is clicked', () => {
        const removeSpy = sandbox.spy(testProps, 'removeItem');
        const cartItem = shallow(
            <CartItem item={testProps.item} removeItem={testProps.removeItem} />,
        );

        cartItem.find('Button').simulate('click');

        expect(removeSpy.calledOnce).to.equal(true);
    });
});
