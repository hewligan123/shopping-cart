/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import CartList from '../../../src/components/CartList/CartList';

configure({ adapter: new Adapter() });

describe('CartList', () => {
    const testProps = {
        cart: [{ key: '1' }, { key: '2' }, { key: '3' }],
        removeItem: () => {},
        total: 2.5,
    };

    it('Should list the items in the cart', () => {
        const cartList = shallow(
            <CartList cart={testProps.cart} removeItem={testProps.removeItem} total={testProps.total} />,
        );

        expect(cartList.find('CartItem').length).to.equal(testProps.cart.length);
    });

    it('Should display the total price of the items in the cart', () => {
        const cartList = shallow(
            <CartList cart={testProps.cart} removeItem={testProps.removeItem} total={testProps.total} />,
        );

        expect(cartList.contains('$2.50')).to.equal(true);
    });
});
