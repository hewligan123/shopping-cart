/* globals describe, it */

import React from 'react';

import { expect } from 'chai';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ProductList from '../../../src/components/ProductList/ProductList';

configure({ adapter: new Adapter() });

describe('CartList', () => {
    const testProps = {
        products: [{ key: '1' }, { key: '2' }, { key: '3' }],
        addItem: () => {},
    };


    it('Should list the available product items', () => {
        const productList = shallow(
            <ProductList products={testProps.products} addItem={testProps.addItem} />,
        );

        expect(productList.find('ProductItem').length).to.equal(testProps.products.length);
    });
});
