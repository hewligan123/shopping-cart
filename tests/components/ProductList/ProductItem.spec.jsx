/* globals describe, it, beforeEach, afterEach */

import React from 'react';

import { expect } from 'chai';
import sinon from 'sinon';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import ProductItem from '../../../src/components/ProductList/ProductItem';

configure({ adapter: new Adapter() });

describe('ProductItem', () => {
    const testProps = {
        item: {
            key: 'TEST_KEY',
            name: 'TEST_NAME',
            price: 2.5,
        },
        addItem: () => {},
    };

    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('Should display the name of the item', () => {
        const productItem = shallow(
            <ProductItem item={testProps.item} addItem={testProps.addItem} />,
        );

        expect(productItem.contains(testProps.item.name)).to.equal(true);
    });

    it('Should display the price of the item', () => {
        const productItem = shallow(
            <ProductItem item={testProps.item} addItem={testProps.addItem} />,
        );

        expect(productItem.contains('$2.50')).to.equal(true);
    });

    it('Should call the add item function when the add button is clicked', () => {
        const addSpy = sandbox.spy(testProps, 'addItem');
        const productItem = shallow(
            <ProductItem item={testProps.item} addItem={testProps.addItem} />,
        );

        productItem.find('Button').simulate('click');

        expect(addSpy.calledOnce).to.equal(true);
    });
});
