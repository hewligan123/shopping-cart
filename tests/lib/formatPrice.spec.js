/* globals describe, it */

import { expect } from 'chai';

import formatPrice from '../../src/lib/formatPrice';

describe('formatPrice', () => {
    it('Should format a number as dollars and cents', () => {
        const value = 55.5;
        expect(formatPrice(value)).to.equal('$55.50');
    });

    it('Should round prices with too many decimal places', () => {
        const value = 55.511;
        expect(formatPrice(value)).to.equal('$55.51');
    });
});
